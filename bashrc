#!/usr/bin/env bash

# Shell prompt based on the Solarized Dark theme.
# Created by Tobante 2017

[ -n "$PS1" ] && source ~/.bash_profile;
