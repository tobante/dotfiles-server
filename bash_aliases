# SYSTEM
alias c='clear'
alias ls='ls -alG'

# EDITOR
alias vi='vim'

# GIT 
alias gpom='git push -u origin master'
alias gits='git status'
