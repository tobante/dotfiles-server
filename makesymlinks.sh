#!/usr/local/bin/bash
############################
# makesymlinks.sh
# This script creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
# Created by Tobante 2017
############################

########## Variables
dir=~/dotfiles-server                    # dotfiles directory
olddir=~/.dotfiles_bk             # old dotfiles backup directory
files="bashrc bash_prompt bash_profile bash_aliases vimrc"    # list of files/folders to symlink in homedir


# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
echo "...done"

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file ~/$olddir/
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
done
